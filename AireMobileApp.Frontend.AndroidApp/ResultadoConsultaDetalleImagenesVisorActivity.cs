﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "ResultadoConsultaDetalleImagenesVisorActivity")]
    public class ResultadoConsultaDetalleImagenesVisorActivity : Activity
    {
        string path;
        File _file;
        ImageView _imageviewDescarga;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ResultadoConsultaDetalleImagenesVisor);
            InitializeEvents();
            _imageviewDescarga = FindViewById<ImageView>(Resource.Id.imageViewDescarga);
            path = Intent.GetStringExtra("path") ?? "";
            _file = new File(path);
            Android.Net.Uri contentUri = Android.Net.Uri.FromFile(_file);
            _imageviewDescarga.SetImageURI(contentUri);
            
        }

        private void InitializeControls()
        {

          
            InitializeEvents();
        }

        private void InitializeEvents()
        {
          
        }
    }
}