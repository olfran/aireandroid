﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "VisorMultimediaActivity")]
    public class VisorMultimediaActivity : Activity
    {
        string path;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.VisorMultimedia);
            path = Intent.GetStringExtra("path") ?? "";
            // Create your application here
            var videoView = FindViewById<VideoView>(Resource.Id.videoView1);
            var videoControl = FindViewById<MediaController>(Resource.Id.mediaController1);
            videoView.SetVideoPath(path);
            videoControl = new Android.Widget.MediaController(this);
            videoControl.SetMediaPlayer(videoView);
            videoView.SetMediaController(videoControl);
            videoView.RequestFocus();
            videoView.Visibility = ViewStates.Visible;
            videoView.Start();

        }
    }
}