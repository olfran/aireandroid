﻿// By Olfran Jiménez <olfran@gmail.com>

using Android.Content.Res;
using Android.Widget;
using System;
using System.Text.RegularExpressions;

namespace AireMobileApp.Frontend.AndroidApp.Validations
{
    /// <summary>
    /// Validar los EditTexts en Android y añadir el error al EditText en caso de error
    /// Los métodos retornan true: es válido, false: no es válido
    /// </summary>
    public class ValidationManager
    {
        private Resources _resources;

        public ValidationManager(Resources resources)
        {
            _resources = resources;
        }

        /// <summary>
        /// Validar que no esté vacío
        /// </summary>
        /// <param name="edit">EditText</param>
        /// <returns>true: es válido, false: no es válido</returns>
        public bool NotEmpty(params EditText[] edit)
        {
            bool valid = false;

            for (int i = 0; i < edit.Length; i++)
            {
                valid = !string.IsNullOrEmpty(edit[i].Text);

                if (!valid)
                {
                    edit[i].Error = _resources.GetString(Resource.String.ErrorEmpty);
                    edit[i].RequestFocus();

                    return false;
                }
            }

            return valid;
        }

        public bool Url(params EditText[] edit)
        {
            bool valid = false;

            for (int i = 0; i < edit.Length; i++)
            {
                valid = Uri.IsWellFormedUriString(edit[i].Text, UriKind.Absolute);

                if (!valid)
                {
                    edit[i].Error = _resources.GetString(Resource.String.ErrorUrl);
                    edit[i].RequestFocus();

                    return false;
                }
            }

            return valid;
        }
    }
}