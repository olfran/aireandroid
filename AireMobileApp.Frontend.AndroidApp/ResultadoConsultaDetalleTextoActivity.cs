﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "ResultadoConsultaDetalleTextoActivity")]
    public class ResultadoConsultaDetalleTextoActivity : Activity
    {
        ListView _listviewTextos;
        TextView _textIdDocumento;
        string _directoriotextos;
        List<string> _archivosImg;
        ListAdapterText listAdapterTexto;
        List<ClassListados> items;
        ClassListados registroitem;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ResultadoConsultaDetalleTextos);
            _directoriotextos = ConstantesManejoArchivo.directorioDEscargaTexto;
            _listviewTextos = FindViewById<ListView>(Resource.Id.listViewTextoDescargadas);
            _textIdDocumento = FindViewById<TextView>(Resource.Id.textViewNroRefTexto);
            string textIddocumento = Intent.GetStringExtra("id_documento") ?? "Dato no disponible";
            var lista = new FilesHelpers();
            _archivosImg = lista.LeerArchivos(_directoriotextos, "texto");
            int i = 1;
            items = new List<ClassListados>();
            foreach (string value in _archivosImg)
            {
                registroitem = new ClassListados() { string1 = i.ToString(), string2 = value };
                items.Add(registroitem);
                i++;
            }
            listAdapterTexto = new ListAdapterText(this, items);
            var _listviewImagenes = FindViewById<ListView>(Resource.Id.listViewTextoDescargadas);
            _listviewTextos.Adapter = listAdapterTexto;
            _listviewTextos.ItemClick += _listviewTextos_ItemClick;
        }




        private void _listviewTextos_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            string path = "";
            int pos = e.Position;
            path = _archivosImg[pos];
            Intent activity01 = new Intent(this, typeof(VisorTxtActivity));
            activity01.PutExtra("path", path);
            StartActivity(activity01);
        }
    }
}