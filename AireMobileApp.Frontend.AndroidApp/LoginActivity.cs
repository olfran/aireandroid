﻿using Android.App;
using Android.Widget;
using Android.OS;
using AireMobileApp.Frontend.AndroidApp.Validations;
using System;
using System.Threading.Tasks;
using Android.Views;
using AireMobileApp.Frontend.Proxy.Login;
using Android.Content;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "@string/Login", MainLauncher = true, Icon = "@drawable/icon")]
    public class LoginActivity : Activity
    {
        Button _buttonLogin;
        EditText _editUsername, _editPassword;
        RelativeLayout _loadingLayout;
        ValidationManager _validate;
        AireConfig _config;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Login);
            _config = new AireConfig(this);

            InitializeControls();

            _editUsername.Text = _config.Username;
            _editPassword.Text = _config.Password;
        }

        /// <summary>
        /// Inicializar los controles
        /// </summary>
        private void InitializeControls()
        {
            _validate = new ValidationManager(Resources);

            _buttonLogin = FindViewById<Button>(Resource.Id.buttonLogin);
            _editUsername = FindViewById<EditText>(Resource.Id.editUsername);
            _editPassword = FindViewById<EditText>(Resource.Id.editPassword);
            _loadingLayout = FindViewById<RelativeLayout>(Resource.Id.loadingLayout);

            InitializeEvents();
        }

        /// <summary>
        /// Inicializar los eventos
        /// </summary>
        private void InitializeEvents()
        {
            _buttonLogin.Click += ButtonLoginClick;
        }

        //Eventos
        private async void ButtonLoginClick(object sender, EventArgs e)
        {
            bool isValid = _validate.NotEmpty(_editUsername, _editPassword);

            if (string.IsNullOrEmpty(_config.UrlAire))
            {
                Toast.MakeText(this,
                    Resources.GetString(Resource.String.ErrorUrlAireMissing),
                    ToastLength.Long).Show();

                Intent intent = new Intent(this, typeof(LoginConfigActivity));
                StartActivity(intent);
            }

            if (isValid)
            {
                try
                {
                    await LoginAire();
                }
                catch
                {
                    Toast.MakeText(this,
                        Resources.GetString(Resource.String.ErrorNetwork),
                        ToastLength.Long).Show();
                    Loading(false);
                }
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            var configure = menu.FindItem(Resource.Id.itemLoginConfigure);

            MenuInflater.Inflate(Resource.Menu.Login, menu);

            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.itemLoginConfigure:
                    Intent intent = new Intent(this, typeof(LoginConfigActivity));
                    StartActivity(intent);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        /// <summary>
        /// Tratar de hacer login en AIRE
        /// </summary>
        private async Task LoginAire()
        {
            Loading(true);

            var login = new LoginAire(_config.UrlAire);
            var result = await login.LoginAsync(_editUsername.Text, _editPassword.Text);

            //Si el login fue Ok
            if (result.Ok)
            {
                _config.Login = _editUsername.Text;
                _config.Username = result.Username;
                _config.Password = _editPassword.Text;
                _config.Token = result.Token;

                Intent intent = new Intent(this, typeof(SelectCatalogoActivity));
                StartActivity(intent);
                Finish();
            }
            else
            {
                Toast.MakeText(this,
                    Resources.GetString(Resource.String.ErrorUserPassword),
                    ToastLength.Long).Show();
            }

            Loading(false);
        }

        /// <summary>
        /// Mostrar u ocultar el Loading
        /// </summary>
        /// <param name="show">true mostrar, false ocultar</param>
        private void Loading(bool show)
        {
            _loadingLayout.Visibility = show ? ViewStates.Visible : ViewStates.Gone;
            _buttonLogin.Enabled = !show;
        }
    }
}
