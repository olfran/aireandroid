﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AireMobileApp.Frontend.Proxy.DataAccess;
using System.Threading.Tasks;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "@string/ResultGeneral", Icon = "@drawable/icon")]
    public class ResultadoConsultaDetalleActivity : Activity
    {
        AireConfig _config;
        DocumentoRepository _documentoRepository;
        string idDocumento = "";
        RelativeLayout _loadingConsultaDetalle;
        TextView _textNumrefConsultaGeneral;
        Button _buttonImagen;
        Button _buttonArchivo;
        Button _buttonTexto;
        Button _buttonMultimedia;
        Button _buttonFlujoTrabajo;
        Button _buttonDetalleEnvio;
        LinearLayout _layoutConsultaDetalle;
        string numref;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ResultadoConsultaDetalle);
            _config = new AireConfig(this);
            _documentoRepository = new DocumentoRepository(_config.UrlAire, _config.Token);

            //Añadir el botón "atrás" en la barra de títulos
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetDisplayShowHomeEnabled(true);

            //Obtener el query de consulta general
            idDocumento = Intent.GetStringExtra("id_documento") ?? "";
            numref = Intent.GetStringExtra("numref") ?? "";

            InitializeControls();

            Task.Factory.StartNew(() => BuscarAdjuntos(idDocumento));
            _buttonArchivo.Click += _buttonArchivo_Click;
            _buttonImagen.Click += _buttonImagen_Click;
            _buttonMultimedia.Click += _buttonMultimedia_Click;
        }

        private void _buttonMultimedia_Click(object sender, EventArgs e)
        {
            var activity01 = new Intent(this, typeof(ResultadoConsultaDetalleAudiosActivity));
            activity01.PutExtra("id_documento", idDocumento);
            activity01.PutExtra("numref", numref);
            StartActivity(activity01);
        }

        private void _buttonImagen_Click(object sender, EventArgs e)
        {
          
            var activity01 = new Intent(this, typeof(ResultadoConsultaDetalleImagenesActivity));
            activity01.PutExtra("id_documento", idDocumento);
            activity01.PutExtra("numref", numref);
            StartActivity(activity01);
        }

        private void _buttonArchivo_Click(object sender, EventArgs e)
        {
            var activity02 = new Intent(this, typeof(ResultadoConsultaDetalleTextoActivity));
            activity02.PutExtra("id_documento", idDocumento);
            activity02.PutExtra("numref", numref);
            StartActivity(activity02);
        }

        private void InitializeControls()
        {
            _layoutConsultaDetalle = FindViewById<LinearLayout>(Resource.Id.layoutConsultaDetalle);
            _loadingConsultaDetalle = FindViewById<RelativeLayout>(Resource.Id.loadingConsultaDetalle);
            _textNumrefConsultaGeneral = FindViewById<TextView>(Resource.Id.textNumrefConsultaGeneral);
            _buttonImagen = FindViewById<Button>(Resource.Id.buttonImagen);
            _buttonArchivo = FindViewById<Button>(Resource.Id.buttonArchivo);
            _buttonTexto = FindViewById<Button>(Resource.Id.buttonTexto);
            _buttonMultimedia = FindViewById<Button>(Resource.Id.buttonMultimedia);
            _buttonFlujoTrabajo = FindViewById<Button>(Resource.Id.buttonFlujoTrabajo);
            _buttonDetalleEnvio = FindViewById<Button>(Resource.Id.buttonDetalleEnvio);

            _textNumrefConsultaGeneral.Text = numref;
        }

        public void BuscarAdjuntos(string idDocumento)
        {
            var adjuntos = _documentoRepository.GetAdjuntos(_config.IdCatalogo, idDocumento).Result;

            RunOnUiThread(() =>
            {
                if (adjuntos != null)
                {
                    foreach (var adjunto in adjuntos)
                    {
                        switch (adjunto.Tipo)
                        {
                            case Proxy.DataAccess.Models.TipoAdjunto.Imagen:
                                _buttonImagen.SetBackgroundColor(Android.Graphics.Color.CornflowerBlue);
                                break;
                            case Proxy.DataAccess.Models.TipoAdjunto.Archivo:
                                _buttonArchivo.SetBackgroundColor(Android.Graphics.Color.CornflowerBlue);
                                break;
                            case Proxy.DataAccess.Models.TipoAdjunto.Texto:
                                _buttonTexto.SetBackgroundColor(Android.Graphics.Color.CornflowerBlue);
                                break;
                            case Proxy.DataAccess.Models.TipoAdjunto.Multimedia:
                                _buttonMultimedia.SetBackgroundColor(Android.Graphics.Color.CornflowerBlue);
                                break;
                            default:
                                break;
                        }
                    }
                }

                Loading(false);
            });
        }

        public override bool OnMenuItemSelected(int featureId, IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }

            return base.OnMenuItemSelected(featureId, item);
        }

        /// <summary>
        /// Mostrar u ocultar el Loading
        /// </summary>
        /// <param name="show">true mostrar, false ocultar</param>
        private void Loading(bool show)
        {
            _layoutConsultaDetalle.Visibility = !show ? ViewStates.Visible : ViewStates.Gone;
            _loadingConsultaDetalle.Visibility = show ? ViewStates.Visible : ViewStates.Gone;
        }
    }
}