﻿using System;
using System.Collections.Generic;
using System.Linq;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using AireMobileApp.Frontend.AndroidApp.Adapters;
using AireMobileApp.Frontend.Proxy.DataAccess;
using AireMobileApp.Frontend.Proxy.DataAccess.Models;
using System.Threading.Tasks;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "@string/ResultGeneral", Icon = "@drawable/icon")]
    public class ResultadoConsultaActivity : Activity
    {
        AireConfig _config;
        DocumentoRepository _documentoRepository;
        ListView _listViewResultadoGeneral;
        ConsultaGeneralAdapter _listviewAdapter;
        RelativeLayout _loadingLayout, _loadingCambiarPagina;
        Spinner _spinnerPaginas;
        TextView _textViewResultados;
        LinearLayout _layoutResultadoGeneral, _layoutResultadoPagina;
        string query = "";
        bool _esDetallada;
        Button _buttonGoPage;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ResultadoConsulta);
            _config = new AireConfig(this);
            _documentoRepository = new DocumentoRepository(_config.UrlAire, _config.Token);

            InitializeControls();

            //Añadir el botón "atrás" en la barra de títulos
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetDisplayShowHomeEnabled(true);

            //Obtener el query de consulta general
            query = Intent.GetStringExtra("query") ?? "";

            //Verificar si el query es de la búsqueda detallada
            string detallada = Intent.GetStringExtra("detallada") ?? "";

            _esDetallada = !String.IsNullOrEmpty(detallada);
        }

        protected override void OnStart()
        {
            Task.Factory.StartNew(() => Buscar(query, "1"));

            base.OnStart();
        }

        private void InitializeControls()
        {
            _listViewResultadoGeneral = FindViewById<ListView>(Resource.Id.listViewResultadoGeneral);
            _listviewAdapter = new ConsultaGeneralAdapter(this);
            _listViewResultadoGeneral.Adapter = _listviewAdapter;
            _loadingLayout = FindViewById<RelativeLayout>(Resource.Id.loadingLayoutResultadoGeneral);
            _loadingCambiarPagina = FindViewById<RelativeLayout>(Resource.Id.loadingCambiarPagina);

            _layoutResultadoPagina = FindViewById<LinearLayout>(Resource.Id.layoutResultadoPagina);
            _layoutResultadoGeneral = FindViewById<LinearLayout>(Resource.Id.layoutResultadoGeneral);
            _spinnerPaginas = FindViewById<Spinner>(Resource.Id.spinnerPaginas);
            _textViewResultados = FindViewById<TextView>(Resource.Id.textViewResultados);
            _buttonGoPage = FindViewById<Button>(Resource.Id.buttonGoPage);

            InitializeEvents();
        }

        private void InitializeEvents()
        {
            _buttonGoPage.Click += ButtonGoPageClick;
            _listViewResultadoGeneral.ItemClick += ListViewResultadoGeneralItemClick;
        }

        private void ListViewResultadoGeneralItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (e.Position >= 0 && e.Position < _listviewAdapter.Documentos.Count)
            {
                var documento = _listviewAdapter.Documentos[e.Position];

                Intent intent = new Intent(this, typeof(ResultadoConsultaDetalleActivity));

                intent.PutExtra("id_documento", documento.Id.ToString());
                intent.PutExtra("numref", documento.Numref);
                
                StartActivity(intent);
            }
        }

        private void ButtonGoPageClick(object sender, EventArgs e)
        {
            string page = _spinnerPaginas.SelectedItem.ToString();

            Task.Factory.StartNew(() => Buscar(query, page, true));
        }

        private void Buscar(string query, string pagina, bool isPagination = false)
        {
            BusquedaViewModel resultado = _esDetallada ?
                _documentoRepository.BusquedaDetallada(_config.IdCatalogo, query, pagina).Result:
                _documentoRepository.BusquedaAmplia(_config.IdCatalogo, query, pagina).Result;

            RunOnUiThread(() =>
            {
                string result = GetString(Resource.String.Results);

                _listviewAdapter.Documentos = resultado.Documentos;
                _listviewAdapter.NotifyDataSetChanged();

                if (isPagination)
                {
                    LoadingPagination(true);
                }

                _textViewResultados.Text = String.Format(result, resultado.PaginaActual,
                    resultado.TotalPaginas, resultado.TotalItems);

                if (isPagination)
                {
                    LoadingPagination(false);
                }
                else
                {
                    CargarPaginador(resultado.TotalPaginas);
                    Loading(false);
                }
            });
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnMenuItemSelected(int featureId, IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }

            return base.OnMenuItemSelected(featureId, item);
        }

        /// <summary>
        /// Cargar el paginador
        /// </summary>
        private void CargarPaginador(int totalPaginas)
        {
            var paginas = Enumerable.Range(1, totalPaginas).Select(x => x.ToString()).ToList();

            _spinnerPaginas.Adapter = new ArrayAdapter(this,
                Android.Resource.Layout.SimpleSpinnerDropDownItem,
                paginas);
        }

        /// <summary>
        /// Mostrar u ocultar el Loading
        /// </summary>
        /// <param name="show">true mostrar, false ocultar</param>
        private void Loading(bool show)
        {
            _layoutResultadoGeneral.Visibility = !show ? ViewStates.Visible: ViewStates.Gone;
            _loadingLayout.Visibility = show ? ViewStates.Visible : ViewStates.Gone;
        }

        /// <summary>
        /// Mostrar u ocultar el Loading
        /// </summary>
        /// <param name="show">true mostrar, false ocultar</param>
        private void LoadingPagination(bool show)
        {
            _buttonGoPage.Enabled = !show;
            _layoutResultadoPagina.Visibility = !show ? ViewStates.Visible : ViewStates.Gone;
            _loadingCambiarPagina.Visibility = show ? ViewStates.Visible : ViewStates.Gone;
        }
    }
}