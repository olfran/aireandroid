﻿namespace com.xamarin.recipes.filepicker
{
    using AireMobileApp.Frontend.AndroidApp;
    using Android.App;
    using Android.OS;
    using Android.Support.V4.App;

    [Activity(Label = "Alfa", MainLauncher = false, Icon = "@drawable/ic_launcher")]
    public class FilePickerActivity : FragmentActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            this.Title = Intent.GetStringExtra("Encabezado");

            SetContentView(Resource.Layout.file_picker_main);
        }
    }
}
