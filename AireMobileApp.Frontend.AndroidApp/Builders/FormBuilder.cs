﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AireMobileApp.Frontend.Proxy.DataAccess.Models;
using AireMobileApp.Frontend.Proxy.DataAccess.Types;
using static Android.Views.ViewGroup;
using Android.Text;

namespace AireMobileApp.Frontend.AndroidApp.Builders
{
    /// <summary>
    /// Construye un formulario Android a partir de uno de AIRE
    /// </summary>
    public class FormBuilder
    {
        Context _context;

        public FormBuilder(Context context)
        {
            _context = context;
        }

        /// <summary>
        /// Renderiza un formulario AIRE en un LinearLayout
        /// </summary>
        public void BuildForm(List<CampoModel> campos, LinearLayout layout, bool ignorarTesauros = false)
        {
            for (int i = 0; i < campos.Count; i++)
            {
                AddField(campos[i], layout, ignorarTesauros);
            }
        }

        /// <summary>
        /// Construye la query GET de búsqueda para el servicio a partir de un LinearLayout
        /// Ej. CAMPO_1=Valor_1&CAMPO_2=Valor_2
        /// </summary>
        /// <param name="layout"></param>
        /// <returns></returns>
        public string BuildSearchQuery(LinearLayout layout)
        {
            StringBuilder result = new StringBuilder();
            EditText edit;

            for (int i = 0; i < layout.ChildCount; i++)
            {
                edit = layout.GetChildAt(i) as EditText;

                //Por los momentos siempre serán EditTexts
                if (edit != null && !String.IsNullOrEmpty(edit.Text))
                {
                    result.AppendFormat("CAMPO_{0}={1}&", edit.Id, edit.Text);
                }
            }
            
            return result.ToString();
        }

        private void AddField(CampoModel model, LinearLayout layout, bool ignorarTesauros = false)
        {
            switch (model.TipoElemento)
            {
                case TipoCampo.Texto:
                    layout.AddView(BuildLabel(model));

                    //Verificar si es un Tesauro
                    if (model.IdTesauro > 0 && !ignorarTesauros)
                    {
                        layout.AddView(BuildSpinner(model));
                    }
                    else
                    {
                        layout.AddView(BuildEditText(model, InputTypes.ClassText));
                    }
                    
                    break;

                case TipoCampo.Numero:
                    layout.AddView(BuildLabel(model));
                    layout.AddView(BuildEditText(model, InputTypes.ClassNumber));
                    break;

                case TipoCampo.Fecha:
                    layout.AddView(BuildLabel(model));
                    layout.AddView(BuildEditText(model, InputTypes.ClassDatetime));
                    break;

                case TipoCampo.Numref:
                    layout.AddView(BuildLabel(model));
                    layout.AddView(BuildEditText(model, InputTypes.ClassText));
                    break;

                default:
                    break;
            }
        }

        private TextView BuildLabel(CampoModel model)
        {
            var result = new TextView(_context);

            result.Text = model.Nombre;
            result.SetTextColor(Android.Graphics.Color.Black);

            //match_parent
            result.SetWidth(-1);

            return result;
        }

        private EditText BuildEditText(CampoModel model, InputTypes type)
        {
            var result = new EditText(_context);

            result.Id = (int)model.Id;
            result.SetTextColor(Android.Graphics.Color.Black);
            result.InputType = type;

            //match_parent
            result.SetWidth(-1);
            
            return result;
        }

        private View BuildSpinner(CampoModel model)
        {
            var result = new Spinner(_context);

            result.Id = (int)model.Id;

            return result;
        }
    }
}