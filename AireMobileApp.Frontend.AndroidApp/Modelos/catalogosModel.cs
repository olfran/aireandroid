﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AireMobileApp.Frontend.AndroidApp
{
    public partial class catalogosModel
    {
        public string nombre  { get; set; }
        public string descripcion { get; set; }
        public int longitud { get; set; }
        public int cantdecimal { get; set;}
        public string tipo { get; set; }
    }
}