﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "ResultadoConsultaDetalleImagenesActivity")]
    public class ResultadoConsultaDetalleImagenesActivity : Activity
    {
        ListView _listviewImagenes;
        TextView _textIdDocumento;
        string _directorioImagenes;
        List<string> _archivosImg;
        ListAdapterImg listAdapterImagen;
        List<ClassListados> items;
        ClassListados registroitem;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ResultadoConsultaDetalleImagenes);
            // Create your application here
            InitializeControls();
            string textIddocumento = Intent.GetStringExtra("id_documento") ?? "Dato no disponible";
            _textIdDocumento.Text = textIddocumento;
            var lista = new FilesHelpers();
            _archivosImg = lista.LeerArchivos(_directorioImagenes, "imagen");
            int i = 1;
            items = new List<ClassListados>(); 
            foreach (string value in _archivosImg)
            {
                registroitem = new ClassListados() { string1 = i.ToString(), string2 = value, string3 = value };
                items.Add(registroitem);
              i++;
            }
            listAdapterImagen = new ListAdapterImg(this, items);
            var _listviewImagenes = FindViewById<ListView>(Resource.Id.listViewImgDescargadas);
            //ArrayAdapter<string> archivosvideo = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, _archivosImg);
            _listviewImagenes.Adapter = listAdapterImagen;

        }

        private void InitializeControls()
        {
            _directorioImagenes = ConstantesManejoArchivo.directorioDescargaImagenes;
            _listviewImagenes = FindViewById<ListView>(Resource.Id.listViewImgDescargadas);
            _textIdDocumento = FindViewById<TextView>(Resource.Id.textViewNroRef);
            InitializeEvents();
        }

        private void InitializeEvents()
        {
            _listviewImagenes.ItemClick += _listviewImagenes_ItemClick;
        }

        private void _listviewImagenes_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            string path="";
            int pos= e.Position;
            path = _archivosImg[pos];
            Intent activity01 = new Intent(this, typeof(ResultadoConsultaDetalleImagenesVisorActivity));
            activity01.PutExtra("path", path);
            StartActivity(activity01);
        }
    }
}