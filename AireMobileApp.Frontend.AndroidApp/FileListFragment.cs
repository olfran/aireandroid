namespace com.xamarin.recipes.filepicker
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Android.OS;
    using Android.Support.V4.App;
    using Android.Util;
    using Android.Views;
    using Android.Widget;
    using AireMobileApp.Frontend.AndroidApp;

    /// <summary>
    ///   A ListFragment that will show the files and subdirectories of a given directory.
    /// </summary>
    /// <remarks>
    ///   <para> This was placed into a ListFragment to make this easier to share this functionality with with tablets. </para>
    ///   <para> Note that this is a incomplete example. It lacks things such as the ability to go back up the directory tree, or any special handling of a file when it is selected. </para>
    /// </remarks>
    public class FileListFragment : ListFragment
    {
        public static readonly string DefaultInitialDirectory = "/sdcard";
        private FileListAdapter _adapter;
        private DirectoryInfo _directory;
       

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _adapter = new FileListAdapter(Activity, new FileSystemInfo[0]);
            ListAdapter = _adapter;
        }

        public override void OnListItemClick(ListView l, View v, int position, long id)
        {
            var fileSystemInfo = _adapter.GetItem(position);

            if (fileSystemInfo.IsFile())
            {
                // Do something with the file.  In this case we just pop some toast.
               // Log.Verbose("FileListFragment", "The file {0} was clicked.", fileSystemInfo.FullName);
             
                var _fileHelp = new FilesHelpers();
                _fileHelp.agregarArchivoUpload(fileSystemInfo.FullName);
                Toast.MakeText(Activity, "Archivo agregado a la lista " + fileSystemInfo.FullName, ToastLength.Short).Show();
            }
            else
            {
                // Dig into this directory, and display it's contents
                RefreshFilesList(fileSystemInfo.FullName);
            }

            base.OnListItemClick(l, v, position, id);
        }

        public override void OnResume()
        {
            base.OnResume();
            RefreshFilesList(DefaultInitialDirectory);
        }

        public void RefreshFilesList(string directory)
        {
            ConstantesManejoArchivo listadoMime = new ConstantesManejoArchivo();
            string[] extensionFile;
            switch (VariableGlobalesManejoArchivo.TipodeArchivoBuscar)
            {
                case "imagen":
                    extensionFile = listadoMime.extensionImagen;
                    break;
                case "audio":
                    extensionFile = listadoMime.extensionAudio;
                    break;
                case "texto":
                    extensionFile = listadoMime.extensionTexto;
                    break;
                case "multimedia":
                    extensionFile = listadoMime.extensionMultimedia;
                    break;
                case "archivox":
                    extensionFile = listadoMime.extensionNoComun;
                    break;
                default:
                    extensionFile = listadoMime.extensionNoComun;
                    break;
            }



            IList<FileSystemInfo> visibleThings = new List<FileSystemInfo>();
            var dir = new DirectoryInfo(directory);

            try
            {
                foreach (var item in dir.GetFileSystemInfos().Where(item => item.IsVisible()))
                {
                    if (item.IsDirectory())
                    {
                        visibleThings.Add(item);
                    }
                    else if (item.IsFile())
                    {

                        //bool isXmlFile = item.Extension.ToLower().EndsWith("txt");
                        //if (isXmlFile)
                        //{
                        var ext = item.Extension.ToLower().Replace(".","");
                        if (extensionFile.Contains(ext))
                        { 
                            visibleThings.Add(item);
                        }
                        //}

                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("FileListFragment", "No se puede acceder al directorio " + _directory.FullName + "; " + ex);
                Toast.MakeText(Activity, "No se puede acceder al directorio " + directory, ToastLength.Short).Show();
                return;
            }

            _directory = dir;

            _adapter.AddDirectoryContents(visibleThings);

            // If we don't do this, then the ListView will not update itself when then data set 
            // in the adapter changes. It will appear to the user that nothing has happened.
            ListView.RefreshDrawableState();

            Log.Verbose("FileListFragment", "Displaying the contents of directory {0}.", directory);
        }
    }
}
