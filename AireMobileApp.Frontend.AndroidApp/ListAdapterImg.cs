﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;

namespace AireMobileApp.Frontend.AndroidApp
{
    class ListAdapterImg : BaseAdapter
    {
        Activity context;
        public List<ClassListados> items;
        public ListAdapterImg(Activity context, List<ClassListados> items) : base()
        {
            this.context = context;
            this.items = items;

        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];
            var view = (convertView ?? context.LayoutInflater.Inflate(
                Resource.Layout.ListItemImagen, parent, false)) as LinearLayout;
            var imageItem = view.FindViewById(Resource.Id.imageViewImgFrag) as ImageView;
            var textNro = view.FindViewById(Resource.Id.textViewNroFrag) as TextView;
            var textNombre = view.FindViewById(Resource.Id.textViewNombreFrg) as TextView;
            File _file;
            _file = new File(item.string3);
            Android.Net.Uri contentUri = Android.Net.Uri.FromFile(_file);
            textNro.SetText(item.string1, TextView.BufferType.Normal);
            textNombre.SetText(item.string2, TextView.BufferType.Normal);
            imageItem.SetImageURI(contentUri);
            return view;
        }
    }
}