﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "@string/SearchGeneral", Icon = "@drawable/icon")]
    public class ConsultaGeneralActivity : Activity
    {
        AireConfig _config;
        EditText _editBusqueda;
        Button _buttonBuscarGeneral;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ConsultaGeneral);
            _config = new AireConfig(this);

            InitializeControls();

            //Añadir el botón "atrás" en la barra de títulos
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetDisplayShowHomeEnabled(true);
        }

        private void InitializeControls()
        {
            _editBusqueda = FindViewById<EditText>(Resource.Id.editBusquedaGeneral);
            _buttonBuscarGeneral = FindViewById<Button>(Resource.Id.buttonBuscarGeneral);

            InitializeEvents();

            _editBusqueda.RequestFocus();
        }

        private void InitializeEvents()
        {
            _buttonBuscarGeneral.Click += ButtonBuscarGeneralClick;
        }

        private void ButtonBuscarGeneralClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(ResultadoConsultaActivity));
            intent.PutExtra("query", _editBusqueda.Text);

            StartActivity(intent);
        }

        public override bool OnMenuItemSelected(int featureId, IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }

            return base.OnMenuItemSelected(featureId, item);
        }
    }
}