﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AireMobileApp.Frontend.AndroidApp
{
    public  class ConstantesManejoArchivo : Application
    {
        //Constantes
        public const int maximoNroImagenes = 5;
        public const int maximoNroAudios = 5;
        public const int maximoNroTextos = 5;
        //Carpeta por defecto para el navegador de archivos
        public const string carpetaxdefectoaudio = "/sdcard/download/AireAudio";
        public const string carpetaxdefectoimagenes = "/sdcard/download/AirePictures";
        public const string carpetaxdefectotexto = "/sdcard/download/AireTexto";
        public const string carpetaxdefectoVideo = "/sdcard/download/AireVideo";
        //Carpetas de creacion de archivos
        public const string directorioAudio = "AireAudio";
        public const string directorioImagenes = "AirePictures";
        public const string directorioVideo = "AireVideo";
        public const string directorioTexto = "AireTexto";
        //Carpetas de descarga de archivos
        public const string directorioDescargaAudio = "/sdcard/download";
        public const string directorioDescargaImagenes = "/sdcard/download";
        public const string directorioDEscargaTexto = "/sdcard/download";

        //MYME Android
        public string[] extensionImagen = { "jpeg", "bmp", "gif", "jpg", "png" };
        public string[] extensionTexto = { "txt" };
        public string[] extensionAudio = { "aac", "wav", "mp3", "m4a", "3gp", "3gpp" };
        public string[] extensionVideo = { "flv", "mp4", "3gp", "mov", "avi", "wmv", "mpeg" };
        public string[] extensionOffice = {"doc","xls","ppt"};
        public string[] extensionMultimedia = { "aac", "wav", "mp3", "m4a", "3gp", "3gpp", "flv", "mp4", "3gp", "mov", "avi", "wmv", "mpeg" };
        public string[] extensionNoComun = { "aac", "wav", "mp3", "m4a", "3gp", "3gpp", "flv", "mp4", "3gp", "mov", "avi", "wmv", "mpeg" , "doc", "xls", "ppt", "flv", "mp4", "3gp", "mov", "avi", "wmv", "mpeg", "aac", "wav", "mp3", "m4a", "3gp", "3gpp", "jpeg", "bmp", "gif", "jpg", "png" ,"txt"};


    }

    public static class VariableGlobalesManejoArchivo
    {
        public static string TipodeArchivoBuscar;
      //  public static List<string> listadoImagenesGlobal;
      //  public static List<string> listadoAudioGlobal;
      //  public static List<string> listadoTextoGlobal;
      //  public static List<string> listadoVideoGlobal;
      //  public static List<string> listadoOtrosGlobal;

    }

    public static class listadoAudioGlobal
    {
        public static List<string> List { get { return _list; } }

        private static List<string> _list; //

        static listadoAudioGlobal()
        {
          
            _list = new List<string>();
        }


        public static void Record(string value)
        {
            _list.Add(value);
        }

        public static void Delete(string value)
        {
            _list.RemoveAt(_list.IndexOf(value));
        }

        public static void DeleteAt(int pos)
        {
            _list.RemoveAt(pos);
        }

        public static void Clear()
        {
            _list.Clear();
        }

        public static int Count()
        {
            return _list.Count();
        }

        public static bool Find(string Value)
        {
            bool existe = false;
            if (_list.IndexOf(Value)>-1)
            {
                existe = true;
            }
            return existe;
        }

    }

    public static class listadoImagenGlobal
    {
        public static List<string> List { get { return _list; } }

        private static List<string> _list; //

        static listadoImagenGlobal()
        {

            _list = new List<string>();
        }


        public static void Record(string value)
        {
            _list.Add(value);
        }

        public static void Delete(string value)
        {
            _list.RemoveAt(_list.IndexOf(value));
        }


        public static void Clear()
        {
            _list.Clear();
        }

        public static int Count()
        {
            return _list.Count();
        }

        public static bool Find(string Value)
        {
            bool existe = false;
            if (_list.IndexOf(Value) > -1)
            {
                existe = true;
            }
            return existe;
        }

    }

    public static class listadoTextoGlobal
    {
        public static List<string> List { get { return _list; } }

        private static List<string> _list; //

        static listadoTextoGlobal()
        {

            _list = new List<string>();
        }


        public static void Record(string value)
        {
            _list.Add(value);
        }

        public static void Delete(string value)
        {
            _list.RemoveAt(_list.IndexOf(value));
        }

        public static void DeleteAt(int pos)
        {
            _list.RemoveAt(pos);
        }

        public static void Clear()
        {
            _list.Clear();
        }

        public static int Count()
        {
            return _list.Count();
        }

        public static bool Find(string Value)
        {
            bool existe = false;
            if (_list.IndexOf(Value) > -1)
            {
                existe = true;
            }
            return existe;
        }

    }

    public static class listadoOtrosGlobal
    {
        public static List<string> List { get { return _list; } }

        private static List<string> _list; //

        static listadoOtrosGlobal()
        {

            _list = new List<string>();
        }


        public static void Record(string value)
        {
            _list.Add(value);
        }

        public static void Delete(string value)
        {
            _list.RemoveAt(_list.IndexOf(value));
        }


        public static void Clear()
        {
            _list.Clear();
        }

        public static int Count()
        {
            return _list.Count();
        }

        public static bool Find(string Value)
        {
            bool existe = false;
            if (_list.IndexOf(Value) > -1)
            {
                existe = true;
            }
            return existe;
        }

    }

}