﻿// By Olfran Jiménez <olfran@gmail.com>

using AireMobileApp.Frontend.Proxy.Login.Models;
using Android.Content;
using Android.Preferences;

namespace AireMobileApp.Frontend.AndroidApp
{
    /// <summary>
    /// Clase global para las configuraciones
    /// </summary>
    public class AireConfig
    {
        private readonly Context _context;
        private readonly ISharedPreferences _preferences;

        /// <summary>
        /// Url base de la instancia de AIRE
        /// </summary>
        public string UrlAire
        {
            get { return GetString(UrlAireKey);  }
            set
            {
                if (value.EndsWith("/"))
                {
                    SetString(UrlAireKey, value.Remove(value.Length - 1));
                }
                else
                {
                    SetString(UrlAireKey, value);
                }
            }
        }

        /// <summary>
        /// Nombre de usuario en AIRE
        /// </summary>
        public string Username
        {
            get { return GetString(UsernameKey); }
            set { SetString(UsernameKey, value); }
        }

        /// <summary>
        /// Nombre del usuario de Login en AIRE
        /// </summary>
        public string Login
        {
            get { return GetString(LoginKey); }
            set { SetString(LoginKey, value); }
        }

        /// <summary>
        /// Contraseña 
        /// @todo: (Por los momentos Plain Text, después encriptar esto un usuario con root puede ver el password!)
        /// </summary>
        public string Password
        {
            get { return GetString(PasswordKey); }
            set { SetString(PasswordKey, value); }
        }

        /// <summary>
        /// Token del usuario 
        /// </summary>
        public string Token
        {
            get { return GetString(TokenKey); }
            set { SetString(TokenKey, value); }
        }

        /// <summary>
        /// Id del último catálogo seleccionado
        /// </summary>
        public string IdCatalogo
        {
            get { return GetString(IdCatalogoKey); }
            set { SetString(IdCatalogoKey, value); }
        }

        /// <summary>
        /// Nombre del último catálogo seleccionado
        /// </summary>
        public string NombreCatalogo
        {
            get { return GetString(NombreCatalogoKey); }
            set { SetString(NombreCatalogoKey, value); }
        }

        public AireConfig(Context context)
        {
            _context = context;
            _preferences = PreferenceManager.GetDefaultSharedPreferences(_context);
        }

        /// <summary>
        /// Guardar o sobrescribir un valor de configuración
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        public void SetString(string key, string value)
        {
            var editor = _preferences.Edit();

            editor.PutString(key, value);
            editor.Apply();
        }

        /// <summary>
        /// Retorna un valor de configuración
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>Valor de configuración o cadena vacía si hubo error</returns>
        public string GetString(string key)
        {
            return _preferences.GetString(key, "");
        }

        //Constantes
        #region Constantes
        const string UrlAireKey = "url_aire";
        const string UsernameKey = "username";
        const string LoginKey = "login"; 
        const string PasswordKey = "password";
        const string TokenKey = "token";
        const string IdCatalogoKey = "id_catalogo";
        const string NombreCatalogoKey = "name_catalogo";
        #endregion
    }
}