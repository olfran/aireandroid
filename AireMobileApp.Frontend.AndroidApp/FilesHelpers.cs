﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;

namespace AireMobileApp.Frontend.AndroidApp
{
    public class FilesHelpers
    {

        public  List<string> LeerArchivos(string MyPath,string tipo)
        {
            ConstantesManejoArchivo listadoMime = new ConstantesManejoArchivo();
            string[] extensionFile;
            switch (tipo)
            {
                case "imagen":
                    extensionFile = listadoMime.extensionImagen;
                    break;
                case "audio":
                    extensionFile = listadoMime.extensionAudio;
                    break;
                case "texto":
                    extensionFile = listadoMime.extensionTexto;
                    break;
                case "multimedia":
                    extensionFile = listadoMime.extensionMultimedia;
                    break;
                default:
                    extensionFile = listadoMime.extensionTexto;
                    break;
            }


            List<string> listado;
            listado = new List<string>();
            var list = Directory.GetFiles(MyPath, "*");

            if (list.Length > 0)
            {
                for (int i = 0; i < list.Length; i++)
                {
                    string filename = list[i];
                    int lastIndex = filename.LastIndexOf('.');
                    var ext = filename.Substring(lastIndex + 1);
                                     
                    if (extensionFile.Contains(ext))
                    {
                        listado.Add(filename);
                    };
                }
            }
            return listado;
        }


        public void EliminarArchivos(string pathstring)
        {
            var list = Directory.GetFiles(pathstring, "*");

            if (list.Length > 0)
            {
                for (int i = 0; i < list.Length; i++)
                {
                    File.Delete(list[i]);
                }
            }
        }


        public void limpiarListadoArchivosSeleccionados()
        {
            listadoAudioGlobal.Clear();
            listadoImagenGlobal.Clear();
            listadoOtrosGlobal.Clear();
            listadoTextoGlobal.Clear();
            
        }

        public void agregarArchivoUpload(string Ruta)
        {

            switch (VariableGlobalesManejoArchivo.TipodeArchivoBuscar)
            {
                case "imagen":
                    int cont;
                    cont = listadoImagenGlobal.Count();
                    if (cont >= -1)
                    {
                        if (listadoImagenGlobal.Find(Ruta) == false)
                        {
                            listadoImagenGlobal.Record(Ruta);
                        }
                    }
                    break;
                case "audio":
                    cont = listadoAudioGlobal.Count();
                    if (cont >= -1)
                    {
                        if (listadoAudioGlobal.Find(Ruta) == false)
                        {
                            listadoAudioGlobal.Record(Ruta);
                        }
                    }

                    break;
                case "texto":
                    cont = listadoTextoGlobal.Count();
                    if (cont >= -1)
                    {
                        if (listadoTextoGlobal.Find(Ruta) == false)
                        {
                            listadoTextoGlobal.Record(Ruta);
                        }
                    }
                    break;
                case "archivox":
                    cont = listadoOtrosGlobal.Count();
                    if (cont >= -1)
                    {
                        if (listadoOtrosGlobal.Find(Ruta) == false)
                        {
                            listadoOtrosGlobal.Record(Ruta);
                        }
                    }
                    break;
                default:

                    break;
            }
        }
        //public void eliminarArchivoUpload(string Tipo, string Ruta)
        //{
        //    int pos;
        //    switch (VariableGlobalesManejoArchivo.TipodeArchivoBuscar)
        //    {
        //        case "imagen":
        //            int cont;
        //            cont = listadoImagenGlobal.Count();
        //            if (cont >= -1)
        //            {
        //                if (listadoImagenGlobal.Find(Ruta) == false)
        //                {
        //                    listadoImagenGlobal.Record(Ruta);
        //                }
        //            }
        //            break;
        //            if (VariableGlobalesManejoArchivo.listadoImagenesGlobal.Contains(Ruta))
        //            {
        //                pos = VariableGlobalesManejoArchivo.listadoOtrosGlobal.IndexOf(Ruta);
        //                VariableGlobalesManejoArchivo.listadoImagenesGlobal.RemoveAt(pos);
        //            }
        //            break;
        //        case "multimedia":
        //            if (VariableGlobalesManejoArchivo.listadoAudioGlobal.Contains(Ruta))
        //            {
        //                pos = VariableGlobalesManejoArchivo.listadoAudioGlobal.IndexOf(Ruta);
        //                VariableGlobalesManejoArchivo.listadoAudioGlobal.RemoveAt(pos);
        //            }
        //            break;
        //        case "texto":
        //            if (VariableGlobalesManejoArchivo.listadoTextoGlobal.Contains(Ruta))
        //            {
        //                pos = VariableGlobalesManejoArchivo.listadoTextoGlobal.IndexOf(Ruta);
        //                VariableGlobalesManejoArchivo.listadoTextoGlobal.RemoveAt(pos);
        //            }
        //            break;
        //        case "archivox":
        //            if (VariableGlobalesManejoArchivo.listadoOtrosGlobal.Contains(Ruta))
        //            {
        //                pos = VariableGlobalesManejoArchivo.listadoOtrosGlobal.IndexOf(Ruta);
        //                VariableGlobalesManejoArchivo.listadoOtrosGlobal.RemoveAt(pos);
        //            }
        //            break;
        //        default:

        //            break;
        //    }
        //}

    }
}