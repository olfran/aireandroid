﻿using System.Linq;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

using AireMobileApp.Frontend.Proxy.DataAccess;
using Android;
using System;
using AireMobileApp.Frontend.Proxy.DataAccess.Models;
using System.Collections.Generic;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "@string/SelectCatalogTitle", Icon = "@drawable/icon")]
    public class SelectCatalogoActivity : Activity
    {
        TextView _textWelcome;
        Spinner _spinnerCatalogs;
        Button _buttonGenerateDocuments, _buttonSearchCatalogGeneral, _buttonSearchCatalogDetailed;

        CatalogoRepository _catalog;
        AireConfig _config;
        List<CatalogoModel> _catalogs;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.SelectCatalogo);
            _config = new AireConfig(this);

            InitializeControls();
        }

        /// <summary>
        /// Inicializar todo lo necesario
        /// </summary>
        private void InitializeControls()
        {
            _textWelcome = FindViewById<TextView>(Resource.Id.textWelcome);
            _spinnerCatalogs = FindViewById<Spinner>(Resource.Id.spinnerCatalogs);
            _buttonGenerateDocuments = FindViewById<Button>(Resource.Id.buttonGenerateDocuments);
            _buttonSearchCatalogGeneral = FindViewById<Button>(Resource.Id.buttonSearchCatalogGeneral);
            _buttonSearchCatalogDetailed = FindViewById<Button>(Resource.Id.buttonSearchCatalogDetailed);

            _textWelcome.Text = $"{_textWelcome.Text} [{_config.Username}]";
            _catalog = new CatalogoRepository(_config.UrlAire, _config.Token);

            //Llenar el spinner con los catálogos
            LoadCatalogos();

            //Inicializar los eventos
            InitializeEvents();
        }

        private void InitializeEvents()
        {
            _spinnerCatalogs.ItemSelected += SpinnerCatalogsItemSelected;
            _buttonGenerateDocuments.Click += ButtonGenerateDocumentsClick;
            _buttonSearchCatalogGeneral.Click += ButtonSearchCatalogGeneralClick;
            _buttonSearchCatalogDetailed.Click += ButtonSearchCatalogDetailedClick;
        }

        private void ButtonSearchCatalogGeneralClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(ConsultaGeneralActivity));
            StartActivity(intent);
        }

        private void ButtonSearchCatalogDetailedClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(ConsultaDetalladaActivity));
            StartActivity(intent);
        }

        /// <summary>
        /// Al cambiar el Spinner establecer el ID del catálogo en la configuración
        /// </summary>
        private void SpinnerCatalogsItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            _config.IdCatalogo = _catalogs[e.Position].Id.ToString();
            _config.NombreCatalogo = _spinnerCatalogs.SelectedItem.ToString();
        }

        private void ButtonGenerateDocumentsClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(GenerarDocumentoActivity));
            StartActivity(intent);
        }

        /// <summary>
        /// Cargar los catálogos en el Spinner
        /// </summary>
        private async void LoadCatalogos()
        {
            long.TryParse(_config.IdCatalogo, out long idLastCatalog);

            _catalogs = await _catalog.GetListaAsync();

            _spinnerCatalogs.Adapter = new ArrayAdapter(this,
                Android.Resource.Layout.SimpleSpinnerDropDownItem,
                _catalogs);

            //Seleccionar el último catálogo seleccionado en el dispositivo
            if (idLastCatalog > 0)
            {
                for (int i = 0; i < _catalogs.Count; i++)
                {
                    if (_catalogs[i].Id == idLastCatalog)
                    {
                        _spinnerCatalogs.SetSelection(i);
                    }
                }
            }
        }
    }
}
