﻿
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using AireMobileApp.Frontend.Proxy.DataAccess;
using AireMobileApp.Frontend.AndroidApp.Builders;
using System.Threading.Tasks;
using AireMobileApp.Frontend.Proxy.DataAccess.Types;
using Android.Content;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "@string/SearchDetailed")]
    public class ConsultaDetalladaActivity : Activity
    {
        AireConfig _config;
        LinearLayout _formLayout;
        CatalogoRepository _catalog;
        FormBuilder _formBuilder;
        RelativeLayout _loadingConsultaDetallada;
        Button _buttonBusquedaDetallada;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ConsultaDetallada);
            _config = new AireConfig(this);

            InitializeControls();
        }

        /// <summary>
        /// Inicializar todo lo necesario
        /// </summary>
        private void InitializeControls()
        {
            _formLayout = FindViewById<LinearLayout>(Resource.Id.formLayout);
            _loadingConsultaDetallada = FindViewById<RelativeLayout>(Resource.Id.loadingConsultaDetallada);
            _buttonBusquedaDetallada = FindViewById<Button>(Resource.Id.buttonBusquedaDetallada);

            _catalog = new CatalogoRepository(_config.UrlAire, _config.Token);
            _formBuilder = new FormBuilder(this);

            InitializeEvents();

            Loading(true);

            Task.Factory.StartNew(() => BuildForm());
        }

        /// <summary>
        /// Inicializar los eventos
        /// </summary>
        private void InitializeEvents()
        {
            _buttonBusquedaDetallada.Click += ButtonBusquedaDetalladaClick; ;
        }

        private void ButtonBusquedaDetalladaClick(object sender, System.EventArgs e)
        {
            string query = _formBuilder.BuildSearchQuery(_formLayout);

            Intent intent = new Intent(this, typeof(ResultadoConsultaActivity));
            intent.PutExtra("query", query);
            intent.PutExtra("detallada", "true");

            StartActivity(intent);
        }

        private void BuildForm()
        {
            var campos = _catalog.GetCamposAsync(_config.IdCatalogo, FormType.Mobile).Result;

            RunOnUiThread(() => _formBuilder.BuildForm(campos, _formLayout, true));
            RunOnUiThread(() => Loading(false));
        }

        /// <summary>
        /// Mostrar u ocultar el Loading
        /// </summary>
        /// <param name="show">true mostrar, false ocultar</param>
        private void Loading(bool show)
        {
            _buttonBusquedaDetallada.Enabled = !show;
            _loadingConsultaDetallada.Visibility = show ? ViewStates.Visible : ViewStates.Gone;
        }
    }
}