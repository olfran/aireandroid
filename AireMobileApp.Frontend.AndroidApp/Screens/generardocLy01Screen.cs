﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AireMobileApp.Frontend.AndroidApp;
using AireMobileApp.Frontend.Proxy.DataAccess;
using AireMobileApp.Frontend.Proxy.DataAccess.Types;
using AireMobileApp.Frontend.AndroidApp.Builders;
using System.Threading.Tasks;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "generardocLy01Screen",MainLauncher =true)]
    public class generardocLy01Screen : Activity
    {
        Button btnAceptar;
        Spinner spinner;
        string texto;
       // AireConfig _config;
      //  LinearLayout _controlsLayout;
       // CatalogoRepository _catalog;
       // FormBuilder _formBuilder;
      //  RelativeLayout _loadingGenerarDocumento;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.generardocLy01);
            spinner = FindViewById<Spinner>(Resource.Id.spinnerPreFijo);

            //SPINNER
           // spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);
            //asignar el array al control
            var adapter = ArrayAdapter.CreateFromResource(this, Resource.Array.PrefijoArray, Android.Resource.Layout.SimpleSpinnerItem);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;
            //BUTTON
            btnAceptar = FindViewById<Button>(Resource.Id.buttonAceptar);
            btnAceptar.Click += BtnAceptar_Click;
            //_catalog = new CatalogoRepository(_config.UrlAire, _config.Token);
            //_formBuilder = new FormBuilder(this);

           // Loading(true);

           // Task.Factory.StartNew(() => BuildForm());


        }

        //private void BuildForm()
        //{
        //    var campos = _catalog.GetCamposAsync(_config.IdCatalogo, FormType.Mobile).Result;

        //    RunOnUiThread(() => _formBuilder.BuildForm(campos, _controlsLayout));
        //    RunOnUiThread(() => Loading(false));
        //}

        /// <summary>
        /// Mostrar u ocultar el Loading
        /// </summary>
        /// <param name="show">true mostrar, false ocultar</param>
        //private void Loading(bool show)
        //{
        //    _loadingGenerarDocumento.Visibility = show ? ViewStates.Visible : ViewStates.Gone;
        //}

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            var activity2 = new Intent(this, typeof(generardocLy02Screen));
            activity2.PutExtra("tipoDocParam",texto);
            VariableGlobalesManejoArchivo.TipodeArchivoBuscar = "texto";
           
             StartActivity(activity2);
            //Throw new NotImplementedException();
        }

        //private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        //{
        //  spinner = (Spinner)sender;
        //    texto = spinner.GetItemAtPosition(e.Position).ToString();
        //   // string toast = string.Format("el valor es {0}", spinner.GetItemAtPosition(e.Position));
        //   // Toast.MakeText(this, toast, ToastLength.Long).Show();
        //}

    }
}