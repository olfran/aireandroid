﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using AireMobileApp;
using AireMobileApp.Frontend.AndroidApp.Screens;
//librerias


namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "Ver - Asociar")]
    public class generardocLyAsociarScreen : Activity
    {
        Button btnImagen;
        Button btnAudio;
        Button btnTexto;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.generardocLyAsociar);
            // Create your application here
            //Botones
            btnImagen = FindViewById<Button>(Resource.Id.buttonImagen);
            btnAudio = FindViewById<Button>(Resource.Id.buttonAudio);
            btnTexto = FindViewById<Button>(Resource.Id.buttonTexto);
            btnImagen.Click += BtnImagen_Click;
            btnAudio.Click += BtnAudio_Click;
            btnTexto.Click += BtnTexto_Click;

        }

        private void BtnTexto_Click(object sender, EventArgs e)
        {
            var activity3 = new Intent(this, typeof(generardocLyTextoScreen));
            StartActivity(activity3);
        }

        private void BtnAudio_Click(object sender, EventArgs e)
        {
                var activity3 = new Intent(this, typeof(generardocLyAudioScreen));
                StartActivity(activity3);
                //   throw new NotImplementedException();
        
        }

        private void BtnImagen_Click(object sender, EventArgs e)
        {
            var activity3 = new Intent(this, typeof(generardoclyCamaraScreen));
            StartActivity(activity3);
           //  throw new NotImplementedException();
        }

        
    }
}