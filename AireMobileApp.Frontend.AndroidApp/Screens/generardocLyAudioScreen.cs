﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using Java.IO;
using Java.Lang;
using Java.Net;
using Uri = Android.Net.Uri;
using Android.Provider;
using Android.Database;
using AireMobileApp;
//librerias


namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "generardocLyAudioScreen")]
    public class generardocLyAudioScreen : Activity
    {
        MediaRecorder _recorder;
        MediaPlayer _player;
        public Button _start;
        public Button _stop;
        public Button _reproducir;
       // public Button _detener;
        public Button _eliminar;
        public Button _escogeraudio;
        public ListView _ListaAudio;
        string _filename;
        public int audiopos;
        public static File _file;
        public static File _dir;
        public string directorioAudio = "AireAudio";
        private List<string> listadoAudios;
        string path = "";

        enum bloqueo
        {
            inicio,
            grabar,
            reproducir,
            detener,
            pausa
            
        };

        protected override void OnCreate(Bundle savedInstanceState)
        {
           
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.generardocLyAudio);
            _start = FindViewById<Button>(Resource.Id.buttonGrabarAudio);
            _stop = FindViewById<Button>(Resource.Id.buttonDetenerAudio);
            _reproducir = FindViewById<Button>(Resource.Id.buttonReproducirAudio);
          //  _detener = FindViewById<Button>(Resource.Id.buttonDetenerAudio01);
            _eliminar = FindViewById<Button>(Resource.Id.buttonEliminarAudio);
            _ListaAudio = FindViewById<ListView>(Resource.Id.listViewAudios);
            _escogeraudio = FindViewById<Button>(Resource.Id.buttonEscojerAudio);
            listadoAudios = new List<string>();
            bloqueobotones("inicio");


       

            _start.Click += delegate
            {
                
                DirectorioParaAudios();//creo un directorio para el audio grabado
                                                                                
                Intent intent = new Intent(MediaStore.ActionImageCapture); //-generar nuevo nombre archivo audio
                _filename = "Aire_" + Guid.NewGuid().ToString();
                _file = new File(_dir, string.Format("{0}.mp3", _filename));
                path = _file.AbsolutePath;
                _start.Enabled = false;
                _recorder.SetAudioSource(AudioSource.Mic);
                _recorder.SetOutputFormat(OutputFormat.Default);
                _recorder.SetAudioEncoder(AudioEncoder.Default);
                _recorder.SetOutputFile(path);
                _recorder.Prepare();
                _recorder.Start();
                bloqueobotones("grabar");
            };

            _stop.Click += delegate
            {
              
                _start.Enabled = true;
                _recorder.Stop();
                _recorder.Reset();
                Toast.MakeText(this, "audio grabado con exito", ToastLength.Short).Show();
                bloqueobotones("detener");

                listadoAudioGlobal.Record(path);
                listadoAudios = listadoAudioGlobal.List;
                ArrayAdapter<string> archivosAudio = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoAudios);
                _ListaAudio.Adapter = archivosAudio;

                //listadoAudios.Add(path);
                //ArrayAdapter<string> archivosAudio = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1,listadoAudios );
                //_ListaAudio.Adapter = archivosAudio;
              
            };


            _escogeraudio.Click += delegate {
                //var imagenIntent = new Intent();
                //imagenIntent.SetType("audio/*");
                //imagenIntent.SetAction(Intent.ActionGetContent);
                //StartActivityForResult(Intent.CreateChooser(imagenIntent, "Escojer audio"), 103);
                var activity2 = new Intent(this, typeof(com.xamarin.recipes.filepicker.FilePickerActivity));
                activity2.PutExtra("Encabezado", "Seleccione archivo audio");
                VariableGlobalesManejoArchivo.TipodeArchivoBuscar = "audio";
                StartActivity(activity2);
            };

            _reproducir.Click += (IntentSender, e) =>
            {
                int cont = listadoAudioGlobal.Count();
                if ( cont> 0)
                {
                    //_player.Reset();
                    //_player.SetDataSource(path);
                    //_player.Prepare();
                    //_player.Start();
                    //string info = _player.Duration.ToString();
                    //Toast.MakeText(this, "Reproduciendo archivo " + info, ToastLength.Short).Show();
                    //bloqueobotones("reproducir");
                    var activity3 = new Intent(this, typeof(VisorMultimediaActivity));
                    activity3.PutExtra("path", path);
                    StartActivity(activity3);

                }
                else
                {
                    Toast.MakeText(this, "No hay nada que reproducir", ToastLength.Short).Show();
                }
            };

            //_detener.Click += (IntentSender, e)=>
            //{
            //    _player.Pause();
            //    bloqueobotones("pausa");
               
            //};

            _eliminar.Click += _eliminar_Click;

            _ListaAudio.ItemClick += _ListaAudio_ItemClick;

            
        }

        private void _eliminar_Click(object sender, EventArgs e)
        {
            if (_ListaAudio.Count > 0)
            {
                listadoAudioGlobal.Delete(listadoAudios[audiopos].ToString());
                listadoAudios = listadoAudioGlobal.List;
                ArrayAdapter<string> archivosAudio = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoAudios);
                _ListaAudio.Adapter = archivosAudio;
            }
        }

        private void _ListaAudio_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (_ListaAudio.Count > 0)
            {
                audiopos = e.Position;
                path = this.listadoAudios[e.Position].ToString();
            }
            else
            {
                path = "";
            }
        }

      
        protected override void OnResume()
        {
            base.OnResume();

            _recorder = new MediaRecorder();
            _player = new MediaPlayer();

            _player.Completion += (sender, e) =>
            {
                _player.Reset();
            };

            }

            protected override void OnPause()
        {
            base.OnPause();

            _player.Release();
            _recorder.Release();
            _player.Dispose();
            _recorder.Dispose();
            _player = null;
            _recorder = null;
        }

        private void DirectorioParaAudios()
        {
            _dir = new File(
                Android.OS.Environment.GetExternalStoragePublicDirectory(
                    Android.OS.Environment.DirectoryMusic), directorioAudio);
            if (!_dir.Exists())
            {
                _dir.Mkdirs();
            }
        }

        private void bloqueobotones(string operacion)
        {
            switch (operacion)
            {
                case "grabar" :
                    _start.Enabled = false;
                    _stop.Enabled =true;
                    _reproducir.Enabled = false;
                  //  _detener.Enabled = false;
                    _eliminar.Enabled = false;
                    _ListaAudio.Enabled = false;
                    break;  
                case "detener":
                    _start.Enabled = true;
                    _stop.Enabled = false;
                    _reproducir.Enabled = true;
                //    _detener.Enabled = true;
                    _eliminar.Enabled = true;
                    _ListaAudio.Enabled = true;
                    break;
                case "reproducir":
                    _start.Enabled = false;
                    _stop.Enabled = false;
                    _reproducir.Enabled = false;
                //    _detener.Enabled = true;
                    _eliminar.Enabled = false;
                    _ListaAudio.Enabled = false;
                    break;
                case "pausa":
                    _start.Enabled = true;
                    _stop.Enabled = false;
                    _reproducir.Enabled = true;
                 //   _detener.Enabled = false;
                    _eliminar.Enabled = true;
                    _ListaAudio.Enabled = true;
                    break;
                case "inicio":
                    _start.Enabled = true;
                    _stop.Enabled = false;
                    _reproducir.Enabled = true;
                //    _detener.Enabled = false;
                    _eliminar.Enabled = true;
                    _ListaAudio.Enabled = true;
                    break;

                default:
                    break;
            }
        }

                
        private string GetRealPathFromURI(Uri contentURI)
        {
            ICursor cursor = ContentResolver.Query(contentURI, null, null, null, null);
            cursor.MoveToFirst();
            string documentId = cursor.GetString(0);
            if (documentId.Contains(":")) { 
            documentId = documentId.Split(':')[1];
            }
            cursor.Close();

            cursor = ContentResolver.Query(
            Android.Provider.MediaStore.Audio.Media.ExternalContentUri,
            null, MediaStore.Audio.Media.InterfaceConsts.Id + " = ? ", new[] { documentId }, null);
            cursor.MoveToFirst();
            string path = cursor.GetString(cursor.GetColumnIndex(MediaStore.Audio.Media.InterfaceConsts.Data));
            cursor.Close();

            return path;
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (requestCode == 103 && resultCode == Result.Ok)
            {
                Uri uri = data.Data;
                string ruta = GetRealPathFromURI(uri);
                //  imagenView.SetImageURI(uri);
                listadoAudios.Add(ruta);
                ArrayAdapter<string> archivosAudio = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoAudios);
                _ListaAudio.Adapter = archivosAudio;
                
            }

            
        }

        public override void OnWindowFocusChanged(bool hasFocus)
        {
            base.OnWindowFocusChanged(hasFocus);
            if (hasFocus)
            {
                listadoAudios = listadoAudioGlobal.List;
                ArrayAdapter<string> archivosAudio = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoAudios);
                _ListaAudio.Adapter = archivosAudio;
            }
            
        }



    }
}