﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using Java.IO;
using Java.Lang;
using Java.Net;
using Uri = Android.Net.Uri;
using Android.Provider;
using Android.Database;
using AireMobileApp;
using Android.Webkit;
using Android.Net;

namespace AireMobileApp.Frontend.AndroidApp.Screens
{
    [Activity(Label = "generardocLyTextoScreen")]
    public class generardocLyTextoScreen : Activity
    {
        public Button btnnuevo;
        public Button btnescojer;
        public Button btneliminar;
        public ListView ListaArchivoTexto;
        public static File _file;
        public static File _dir;
        string _filename;
        private List<string> ArraydeTxt;
        public int audiopos;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.generardocLyTexto);
            btnescojer = FindViewById<Button>(Resource.Id.buttonTextoExistente);
            btnnuevo = FindViewById<Button>(Resource.Id.buttonNuevoArchivoTexto);
            btneliminar = FindViewById<Button>(Resource.Id.buttonEliminarTexto);
            ListaArchivoTexto = FindViewById<ListView>(Resource.Id.listViewTexto);

            //btnescojer.Click += delegate {
            //    Intent chooser = new Intent(Intent.ActionGetContent);
            //    _dir = new File(
            //    Android.OS.Environment.GetExternalStoragePublicDirectory(
            //        Android.OS.Environment.DirectoryDocuments),"");
            //    Uri uri = Uri.Parse("/");
            //    //Uri uri = Uri.Parse(_file.Path);
            //    chooser.AddCategory(Intent.CategoryOpenable);
            //    chooser.SetDataAndType(uri, "*/txt");
            //    // startActivity(chooser);
            //    try
            //    {
            //        StartActivityForResult(chooser,103);
            //    }
            //    catch (Android.Content.ActivityNotFoundException ex)
            //    {
            //        ex.PrintStackTrace();
            //    }
            //    //try
            //    //{
            //    //    StartActivityForResult(Intent.CreateChooser(txtIntent, "Select a File to Upload"), 103);

            //    //}
            //    //catch (Android.Content.ActivityNotFoundException ex)
            //    //{
            //    //    ex.PrintStackTrace();
            //    //}
            // };
            btnescojer.Click += Btnescojer_Click;
            btnnuevo.Click += Btnnuevo_Click;
            btneliminar.Click += Btneliminar_Click;
            ListaArchivoTexto.ItemClick += ListaArchivoTexto_ItemClick;


        }

        private void ListaArchivoTexto_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            audiopos = e.Position;
        }

        private void Btneliminar_Click(object sender, EventArgs e)
        {
            int cant = listadoTextoGlobal.Count();
            if (cant>0)
            {
                listadoTextoGlobal.DeleteAt(audiopos);
                ArrayAdapter<string> archivosAudio = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoTextoGlobal.List);
                ListaArchivoTexto.Adapter = archivosAudio;
            }
        }

        public override void OnWindowFocusChanged(bool hasFocus)
        {
            base.OnWindowFocusChanged(hasFocus);
            if (hasFocus)
            {
            
                ArrayAdapter<string> archivosAudio = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoTextoGlobal.List);
                ListaArchivoTexto.Adapter = archivosAudio;
            }

        }

        private void Btnescojer_Click(object sender, EventArgs e)
        {
            var activity2 = new Intent(this, typeof(com.xamarin.recipes.filepicker.FilePickerActivity));
            activity2.PutExtra("Encabezado", "Seleccione archivo text");
            VariableGlobalesManejoArchivo.TipodeArchivoBuscar = "texto";
            StartActivity(activity2);
        }

        private void Btnnuevo_Click(object sender, EventArgs e)
        {
            var activity2 = new Intent(this, typeof(generardocLyTexto01Screen));
            //VariableGlobalesManejoArchivo.TipodeArchivoBuscar = "texto";
            StartActivity(activity2);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
          
            if (requestCode == 103 )
            {
               // _file = new File(Uri);
                //Uri uri = data.Data;
                //string ruta = GetRealPathFromURI(uri);
                ////  imagenView.SetImageURI(uri);
                //listadoAudios.Add(ruta);
                //ArrayAdapter<string> archivosAudio = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoAudios);
                //_ListaAudio.Adapter = archivosAudio;

            }


        }

    }

  
}