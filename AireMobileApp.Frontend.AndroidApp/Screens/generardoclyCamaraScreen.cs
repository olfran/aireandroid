﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
//librerias
using Android.Provider;
using Java.IO;
using Android.Graphics;
using Android.Content.PM;
using Uri = Android.Net.Uri;
using Camera;
using Android.Database;
using AireMobileApp.Frontend.AndroidApp;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "generardoclyCamaraScreen")]
    public class generardoclyCamaraScreen : Activity
    {
        Button btnimagen;
        Button btnimagengaleria;
        Button btneliminar;
        ImageView imagenView;
        ListView ListaImg;
        public static File _file;
        public static File _dir;
        string _filename;
        private List<string> listadoImagenes;
        public string directorioFotos = "AirePictures";
        int imagpos;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.generardocLyCamara);

            imagpos = -1;
            ListaImg = FindViewById<ListView>(Resource.Id.listViewFotos);
            listadoImagenes = new List<string>();
            btneliminar = FindViewById<Button>(Resource.Id.buttonEliminar);
            btnimagengaleria = FindViewById<Button>(Resource.Id.buttonTomarFotoGaleria);
            btnimagen = FindViewById<Button>(Resource.Id.buttonTomarFoto);
            imagenView = FindViewById<ImageView>(Resource.Id.imageViewFotoTomada);
            

            //tomar foto de la galeria
            btnimagengaleria.Click += delegate
             {
                 var imagenIntent = new Intent();
                 imagenIntent.SetType("image/*");
                 imagenIntent.SetAction(Intent.ActionGetContent);
                 StartActivityForResult(Intent.CreateChooser(imagenIntent, "Escojer Foto"), 103);
             };
            ListaImg.ItemClick += ListaImg_ItemClick;
            btneliminar.Click += Btneliminar_Click;

            //tomar foto de la camara si existe una aplicacion para tomar fotos
            if (ExisteUnaAppParaTomarFotos())
            {
                DirectorioParaFotos();//creo un directorio para las fotos
                //Eventos
                btnimagen.Click += Btnimagen_Click;
            }
            else
            {
                string texto = Resource.String.FotoAppNoExiste.ToString();
                Toast.MakeText(this,texto, ToastLength.Long).Show();
            }
           
           



        }

        private void Btneliminar_Click(object sender, EventArgs e)
        {
            if (ListaImg.Count > 0)
            {
                listadoImagenGlobal.Delete(listadoImagenes[imagpos].ToString());
                listadoImagenes = listadoImagenGlobal.List;
                //listadoImagenes.RemoveAt(imagpos);

                //((ConstantesManejoArchivo)Application).ListadoImagenesGlobal.Add("1");
                ArrayAdapter<string> archivosvideo = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoImagenes);
                ListaImg.Adapter = archivosvideo;
                imagenView.SetImageResource(0);
            }
        }

        private void ListaImg_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (ListaImg.Count > 0)
            {
                string item = this.listadoImagenes[e.Position].ToString();
                imagpos = e.Position;
                //Make a toast with the item name just to show it was clicked
                _file = new File(item);
                Uri contentUri = Uri.FromFile(_file);
          
                imagenView.SetImageURI(contentUri);
            }
            else
            {
                imagpos = -1;
            }
        }



        private void Btnimagen_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            _filename = "Aire_" + Guid.NewGuid().ToString();
            _file = new File(_dir, string.Format("{0}.jpg",_filename));
            intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(_file));
            StartActivityForResult(intent, 102);
        }


        //Verifico si en el dispositivo Android hay una app que tome fotos
        private bool ExisteUnaAppParaTomarFotos()
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            IList<ResolveInfo> availableActivities =
                PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
            return availableActivities != null && availableActivities.Count > 0;
        }

        private void DirectorioParaFotos()
        {
            _dir = new File(
                Android.OS.Environment.GetExternalStoragePublicDirectory(
                    Android.OS.Environment.DirectoryPictures), directorioFotos);
            if (!_dir.Exists())
            {
                _dir.Mkdirs();
            }
        }

        //la magia
        //https://gist.github.com/ryupold/fe38e5acbe1586681e27

        private string GetRealPathFromURI(Uri contentURI)
        {
            ICursor cursor = ContentResolver.Query(contentURI, null, null, null, null);
            cursor.MoveToFirst();
            string documentId = cursor.GetString(0);
            documentId = documentId.Split(':')[1];
            cursor.Close();

            cursor = ContentResolver.Query(
            Android.Provider.MediaStore.Images.Media.ExternalContentUri,
            null, MediaStore.Images.Media.InterfaceConsts.Id + " = ? ", new[] { documentId }, null);
            cursor.MoveToFirst();
            string path = cursor.GetString(cursor.GetColumnIndex(MediaStore.Images.Media.InterfaceConsts.Data));
            cursor.Close();

            return path;
        }


        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (requestCode == 102 && resultCode == Result.Ok)
            {

                // Validar el nuevo directorio en la galeria
                Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
                Uri contentUri = Uri.FromFile(_file);
                mediaScanIntent.SetData(contentUri);
                SendBroadcast(mediaScanIntent);


                //Convertir Imagen
                int height = imagenView.Height;
                int width = Resources.DisplayMetrics.WidthPixels;
                using (Bitmap bitmap = _file.Path.LoadAndResizeBitmap(width, height))
                {
                    //View ImageView
                    imagenView.RecycleBitmap();
                    imagenView.SetImageBitmap(bitmap);
                }

                //llenar el listview
                //listadoImagenes.Add(_filename + ".jpg");

                listadoImagenGlobal.Record(_file.AbsolutePath.ToString());
                listadoImagenes = listadoImagenGlobal.List;
                ArrayAdapter<string> archivosvideo = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoImagenes);
                ListaImg.Adapter = archivosvideo;
                

                //listadoImagenes.Add(_file.AbsolutePath.ToString());
                //ArrayAdapter<string> archivosvideo = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoImagenes);
                //ListaImg.Adapter = archivosvideo;

            }

            if (requestCode == 103 && resultCode == Result.Ok)
            {
                Uri uri = data.Data;
                string ruta = GetRealPathFromURI(uri);
                imagenView.SetImageURI(uri);
                //listadoImagenes.Add(ruta);
                //ArrayAdapter<string> archivosvideo = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoImagenes);
                //ListaImg.Adapter = archivosvideo;

                listadoImagenGlobal.Record(ruta);
                listadoImagenes = listadoImagenGlobal.List;
                ArrayAdapter<string> archivosvideo = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoImagenes);
                ListaImg.Adapter = archivosvideo;
            }


        





    }


        public override void OnWindowFocusChanged(bool hasFocus)
        {
            base.OnWindowFocusChanged(hasFocus);
            if (hasFocus)
            {
                listadoImagenes = listadoImagenGlobal.List;
                ArrayAdapter<string> archivosvideo = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listadoImagenes);
                ListaImg.Adapter = archivosvideo;
            }

        }
    }
}