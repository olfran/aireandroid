﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using AireMobileApp;
using System.IO;

namespace AireMobileApp.Frontend.AndroidApp.Screens
{
    [Activity(Label = "Generar documento")]
    public class generardocLyTexto01Screen : Activity
    {
        public Button _guardarTexto;
        public Button _limpiarTexto;
        public EditText _contenidoTxt;
        public static Java.IO.File _file;
        public static Java.IO.File _dir;
        string _filename;
        string path = "";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.generardocLyTexto01);
            // Create your application here
            _contenidoTxt = FindViewById<EditText>(Resource.Id.editText1);
            _guardarTexto = FindViewById<Button>(Resource.Id.buttonGuardarTexto);
            _limpiarTexto = FindViewById<Button>(Resource.Id.buttonLimpiarTexto);


            _guardarTexto.Click += _guardarTexto_Click;
            _limpiarTexto.Click += _limpiarTexto_Click;
        }

        private void _guardarTexto_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_contenidoTxt.Text))
            {
                Toast.MakeText(this, "No se puede grabar un archivo sin texto", ToastLength.Short).Show(); 
            }
            else
            {
                DirectorioParaTexto();
                _filename = "Aire_" + Guid.NewGuid().ToString();
                _file = new Java.IO.File(_dir, string.Format("{0}.txt", _filename));
                path = _file.AbsolutePath;
                string contenido = _contenidoTxt.Text;
                using (var streamWriter = new StreamWriter(path, true))
                {
                    streamWriter.WriteLine(contenido);
                }
                listadoTextoGlobal.Record(path);
                Toast.MakeText(this, "Archivo creado satisfactoriamente: " + path, ToastLength.Long).Show();

            }
            
        }

        private void _limpiarTexto_Click(object sender, EventArgs e)
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle("Desea eliminar el contenido escrito?");
            alert.SetPositiveButton("Si", (senderAlert, args) => {
                _contenidoTxt.Text = "";
            });
            alert.SetNegativeButton("No", (senderAlert, args) => {

            });
            alert.Show();
            ////run the alert in UI thread to display in the screen
            //RunOnUiThread(() => {
            //    alert.Show();
            //});
        }

        private void DirectorioParaTexto()
        {
            _dir = new Java.IO.File(Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDocuments), ConstantesManejoArchivo.directorioTexto);
            
            if (!_dir.Exists())
            {
                _dir.Mkdirs();
            }
        }
    }
}