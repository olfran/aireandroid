﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "VisorTxtActivity")]
    public class VisorTxtActivity : Activity
    {
        string path;
        TextView _TextoNombre;
        TextView _TextoContenido;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.VisorTexto);
            path = Intent.GetStringExtra("path") ?? "";
            _TextoNombre = FindViewById<TextView>(Resource.Id.textViewNombre);
            _TextoContenido = FindViewById<TextView>(Resource.Id.editTextContenido);
            // Create your application here
            string content;
            using (var streamReader = new StreamReader(path))
            {
                content = streamReader.ReadToEnd();
            }
            _TextoContenido.Text = content;
        }
    }
}