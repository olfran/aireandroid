﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "ResultadoConsultaDetalleAudiosActivity")]
    public class ResultadoConsultaDetalleAudiosActivity : Activity
    {
        ListView _listviewAudios;
        TextView _textIdDocumento;
        string _directorioAudios;
        List<string> _archivosImg;
        ListAdapterAudio listAdapterAudio;
        List<ClassListados> items;
        ClassListados registroitem;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ResultadoConsultaDetalleAudios);
            InitializeControls();
            // Create your application here
            string textIddocumento = Intent.GetStringExtra("id_documento") ?? "Dato no disponible";
           // _textIdDocumento.Text = textIddocumento;
            var lista = new FilesHelpers();
            _archivosImg = lista.LeerArchivos(_directorioAudios, "multimedia");
            int i = 1;
            items = new List<ClassListados>();
            foreach (string value in _archivosImg)
            {
                registroitem = new ClassListados() { string1 = i.ToString(), string2 = value };
                items.Add(registroitem);
                i++;
            }
            listAdapterAudio = new ListAdapterAudio(this, items);
            var _listviewImagenes = FindViewById<ListView>(Resource.Id.listViewAudioDescargadas);
            //ArrayAdapter<string> archivosvideo = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, _archivosImg);
            _listviewAudios.Adapter = listAdapterAudio;
            _listviewAudios.ItemClick += _listviewAudios_ItemClick;
        }

        private void _listviewAudios_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            string path = "";
            int pos = e.Position;
            path = _archivosImg[pos];
            Intent activity01 = new Intent(this, typeof(VisorMultimediaActivity));
            activity01.PutExtra("path", path);
            StartActivity(activity01);
        }

        private void InitializeControls()
        {
            _directorioAudios = ConstantesManejoArchivo.directorioDescargaAudio;
            _listviewAudios = FindViewById<ListView>(Resource.Id.listViewAudioDescargadas);
            _textIdDocumento = FindViewById<TextView>(Resource.Id.textViewNroRefAudio);
            InitializeEvents();
        }

        private void InitializeEvents()
        {
           // _listviewImagenes.ItemClick += _listviewImagenes_ItemClick;
        }


    }
}