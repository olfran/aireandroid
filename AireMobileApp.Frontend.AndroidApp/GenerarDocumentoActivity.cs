﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AireMobileApp.Frontend.Proxy.DataAccess;
using AireMobileApp.Frontend.Proxy.DataAccess.Types;
using AireMobileApp.Frontend.AndroidApp.Builders;
using System.Threading.Tasks;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "@string/GenerateDocuments")]
    public class GenerarDocumentoActivity : Activity
    {
        AireConfig _config;
        LinearLayout _controlsLayout;
        CatalogoRepository _catalog;
        FormBuilder _formBuilder;
        RelativeLayout _loadingGenerarDocumento;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.GenerarDocumento);
            _config = new AireConfig(this);

            InitializeControls();
        }

        /// <summary>
        /// Inicializar todo lo necesario
        /// </summary>
        private void InitializeControls()
        {
            _controlsLayout = FindViewById<LinearLayout>(Resource.Id.controlsLayout);
            _loadingGenerarDocumento = FindViewById<RelativeLayout>(Resource.Id.loadingGenerarDocumento);

            _catalog = new CatalogoRepository(_config.UrlAire, _config.Token);
            _formBuilder = new FormBuilder(this);

            Loading(true);
           
            Task.Factory.StartNew(() => BuildForm());
        }

        private void BuildForm()
        {
            var campos = _catalog.GetCamposAsync(_config.IdCatalogo, FormType.Mobile).Result;

            RunOnUiThread(() => _formBuilder.BuildForm(campos, _controlsLayout));
            RunOnUiThread(() => Loading(false));
        }

        /// <summary>
        /// Mostrar u ocultar el Loading
        /// </summary>
        /// <param name="show">true mostrar, false ocultar</param>
        private void Loading(bool show)
        {
            _loadingGenerarDocumento.Visibility = show ? ViewStates.Visible : ViewStates.Gone;
        }
    }
}