﻿using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;
using AireMobileApp.Frontend.Proxy.DataAccess.Models;

namespace AireMobileApp.Frontend.AndroidApp.Adapters
{
    public class ConsultaGeneralAdapter : BaseAdapter
    {
        public IList<DocumentoModel> Documentos { get; set; }
        readonly Context _context;

        public ConsultaGeneralAdapter(Context context)
        {
            _context = context;
            Documentos = new List<DocumentoModel>();
        }

        public override int Count => Documentos != null ? Documentos.Count : 0;

        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (position >= Documentos.Count)
            {
                position = Documentos.Count - 1;
            }

            var documento = Documentos[position];

            convertView = LayoutInflater.From(_context)
                .Inflate(Resource.Layout.ResultadoConsultaGeneral, null);

            TextView textNumref = convertView.FindViewById<TextView>(Resource.Id.textNumref);
            TextView textFechaGeneracion = convertView.FindViewById<TextView>(Resource.Id.textFechaGeneracion);
            TextView textAdjuntos = convertView.FindViewById<TextView>(Resource.Id.textAdjuntos);

            textNumref.Text = documento.Numref;
            textFechaGeneracion.Text = documento.FechaGeneracion.ToLongDateString();
            textAdjuntos.Text = $"{documento.TotalAdjuntos}";

            return convertView;
        }
    }
}