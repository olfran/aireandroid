﻿using System;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using AireMobileApp.Frontend.AndroidApp.Validations;

namespace AireMobileApp.Frontend.AndroidApp
{
    [Activity(Label = "@string/Configuration")]
    public class LoginConfigActivity : Activity
    {
        private EditText _editUrlAire;
        private Button _buttonAcceptConfig;
        private AireConfig _config;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.LoginConfig);
            _config = new AireConfig(this);

            InitializeControls();

            _editUrlAire.Text = "http://tecnologiaygestiondocumental.info/mobile";
        }

        /// <summary>
        /// Inicializar los controles
        /// </summary>
        private void InitializeControls()
        {
            _editUrlAire = FindViewById<EditText>(Resource.Id.editUrlAire);
            _buttonAcceptConfig = FindViewById<Button>(Resource.Id.buttonAcceptConfig);

            _editUrlAire.Text = _config.UrlAire;

            //Añadir el botón "atrás" en la barra de títulos
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetDisplayShowHomeEnabled(true);

            InitializeEvents();
        }

        /// <summary>
        /// Inicializar los eventos
        /// </summary>
        private void InitializeEvents()
        {
            _buttonAcceptConfig.Click += ButtonAcceptConfigClick;
        }

        private void ButtonAcceptConfigClick(object sender, EventArgs e)
        {
            var validate = new ValidationManager(Resources);

            if (validate.Url(_editUrlAire))
            {
                _config.UrlAire = _editUrlAire.Text;
                Finish();
            }
        }

        public override bool OnMenuItemSelected(int featureId, IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }

            return base.OnMenuItemSelected(featureId, item);
        }
    }
}