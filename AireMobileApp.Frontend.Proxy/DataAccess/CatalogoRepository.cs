﻿// Por Olfran Jiménez <olfran@gmail.com>

using AireMobileApp.Frontend.Proxy.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AireMobileApp.Frontend.Proxy.DataAccess
{
    /// <summary>
    /// Integración de la autenticación con el web service
    /// </summary>
    public class CatalogoRepository : BaseRepository
    {
        /// <summary>
        /// La url debe estar sin la barra final "/"
        /// Ej. https://example.com
        /// </summary>
        /// <param name="url"></param>
        public CatalogoRepository(string url, string token) : base($"{url}/api/catalogo", token)
        {
        }

        /// <summary>
        /// Retorna una lista de todos los catálogos disponibles
        /// para el usuario
        /// </summary>
        /// <param name="token">Token</param>
        /// <param name="limite">Límite</param>
        /// <returns>Lista de catálogos</returns>
        public async Task<List<CatalogoModel>> GetListaAsync(int limite = 0)
        {
            string url = $"{_url}/lista?limite={limite}";

            return await GetAsync<List<CatalogoModel>>(url);
        }

        /// <summary>
        /// Retorna los campos del formulario
        /// </summary>
        /// <param name="token">Token</param>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="tipoFormulario">Tipo de formulario</param>
        /// <returns>Lista de campos</returns>
        public async Task<List<CampoModel>> GetCamposAsync(string idCatalogo, int tipoFormulario)
        {
            string url = $"{_url}/campos?idcatalogo={idCatalogo}&tipo={tipoFormulario}";

            return await GetAsync<List<CampoModel>>(url);
        }
    }
}