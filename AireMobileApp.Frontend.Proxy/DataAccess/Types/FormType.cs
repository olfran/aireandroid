﻿namespace AireMobileApp.Frontend.Proxy.DataAccess.Types
{
    /// <summary>
    /// Tipos de formularios
    /// </summary>
    public struct FormType
    {
        public const int Mobile = 1;
    }
}
