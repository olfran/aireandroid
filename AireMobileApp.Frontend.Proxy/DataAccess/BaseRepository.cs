﻿// Por Olfran Jiménez <olfran@gmail.com>

using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace AireMobileApp.Frontend.Proxy.DataAccess
{
    /// <summary>
    /// Clase base para los repositorios proxy
    /// </summary>
    public abstract class BaseRepository
    {
        protected readonly string _url;
        private readonly string _token;

        /// <summary>
        /// La url debe estar sin la barra final "/"
        /// Ej. https://example.com
        /// </summary>
        /// <param name="url"></param>
        public BaseRepository(string url, string token)
        {
            _url = url;
            _token = token;
        }

        protected async Task<T> GetAsync<T>(string url)
        {
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Add("Authorization", $"bearer {_token}");

            var response = await client.GetAsync(url);
            string json = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<T>(json);

            return result;
        }
    }
}
