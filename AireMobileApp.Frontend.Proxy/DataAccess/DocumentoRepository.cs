﻿using AireMobileApp.Frontend.Proxy.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AireMobileApp.Frontend.Proxy.DataAccess
{
    public class DocumentoRepository : BaseRepository
    {
        public DocumentoRepository(string url, string token) : base($"{url}/api/documento", token)
        {
        }

        /// <summary>
        ///  Realiza una búsqueda general (todos los campos) en el catálogo especificado
        /// </summary>
        /// <param name="idCatalogo">Id Catálogo</param>
        /// <param name="query">Query de búsqueda</param>
        /// <param name="pagina">Número de página</param>
        /// <returns>Resultado de la búsqueda</returns>
        public async Task<BusquedaViewModel> BusquedaAmplia(string idCatalogo, string query, string pagina)
        {
            string url = $"{_url}/busquedaamplia?idc={idCatalogo}&q={query}&p={pagina}";

            return await GetAsync<BusquedaViewModel>(url);
        }

        /// <summary>
        ///  Realiza una búsqueda general (todos los campos) en el catálogo especificado
        /// </summary>
        /// <param name="idCatalogo">Id Catálogo</param>
        /// <param name="query">Query GET de búsqueda (CAMPO_1=Valor_1...)</param>
        /// <param name="pagina">Número de página</param>
        /// <returns>Resultado de la búsqueda</returns>
        public async Task<BusquedaViewModel> BusquedaDetallada(string idCatalogo, string query, string pagina)
        {
            string url = $"{_url}/buscar?idc={idCatalogo}&p={pagina}&q={query}";

            return await GetAsync<BusquedaViewModel>(url);
        }

        /// <summary>
        /// Retorna una lista de los adjuntos pertenecientes al documento
        /// </summary>
        /// <param name="idCatalogo">Id Catálogo</param>
        /// <param name="idDocumento">Id Documento</param>
        /// <returns>Lista de adjuntos</returns>
        public async Task<List<AdjuntoModel>> GetAdjuntos(string idCatalogo, string idDocumento)
        {
            string url = $"{_url}/getadjuntos?idc={idCatalogo}&idd={idDocumento}";
            var adjuntos = await GetAsync<List<AdjuntoModel>>(url);

            foreach (var adjunto in adjuntos)
            {
                adjunto.Url = string.Format("{0}/{1}", _url, adjunto.Url);
            }

            return adjuntos;
        }
    }
}
