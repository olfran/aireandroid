﻿namespace AireMobileApp.Frontend.Proxy.DataAccess.Models
{
    /// <summary>
    /// Campo de un formulario
    /// </summary>
    public class CampoModel
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public string NombreFisico { get { return $"CAMPO_{Id}"; } }
        public long TipoElemento { get; set; }
        public int Tamano { get; set; }
        public bool Varios { get; set; }
        public long IdTesauro { get; set; }
        public bool Requerido { get; set; }
        public string ValorDefecto { get; set; }
    }
}
