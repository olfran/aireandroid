﻿// By Olfran Jiménez <olfran@gmail.com>

namespace AireMobileApp.Frontend.Proxy.DataAccess.Models
{
    /// <summary>
    /// Modelo simple para un catálogo
    /// </summary>
    public class CatalogoModel
    {
        /// <summary>
        /// Id del catálogo
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Nombre del catálogo
        /// </summary>
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}