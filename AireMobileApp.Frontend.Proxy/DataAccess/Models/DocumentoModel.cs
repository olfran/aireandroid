﻿// By Olfran Jiménez <olfran@gmail.com>

using System;
using System.Collections.Generic;

namespace AireMobileApp.Frontend.Proxy.DataAccess.Models
{
    /// <summary>
    /// Representa un documento
    /// </summary>
    public class DocumentoModel
    {
        public long Id { get; set; }
        public string Numref { get; set; }
        public DateTime FechaGeneracion { get; set; }
        public IList<CampoModel> Campos { get; set; }
        public long TotalAdjuntos { get; set; }
        public long IdFlujo { get; set; }
    }
}