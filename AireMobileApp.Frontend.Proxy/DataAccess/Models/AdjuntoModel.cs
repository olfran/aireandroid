﻿namespace AireMobileApp.Frontend.Proxy.DataAccess.Models
{
    /// <summary>
    /// Representa un adjunto de un documento
    /// </summary>
    public class AdjuntoModel
    {
        /// <summary>
        /// Id del Adjunto
        /// </summary>
        public long IdAdjunto { get; set; }

        /// <summary>
        /// Nombre en pantalla
        /// </summary>
        public string NombrePantalla { get; set; }

        /// <summary>
        /// Nombre físico del archivo
        /// </summary>
        public string NombreFisico { get; set; }

        /// <summary>
        /// Tamaño del Archivo en bytes
        /// </summary>
        public long Tamano { get; set; }

        /// <summary>
        /// Url remota para descargar el archivo
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Extension del archivo
        /// </summary>
        public string Extension { get; set; }

        public TipoAdjunto Tipo
        {
            get
            {
                if (NombreFisico != null)
                {
                    if (NombreFisico.StartsWith("I"))
                    {
                        return TipoAdjunto.Imagen;
                    }

                    if (NombreFisico.StartsWith("A"))
                    {
                        return TipoAdjunto.Archivo;
                    }

                    if (NombreFisico.StartsWith("T"))
                    {
                        return TipoAdjunto.Texto;
                    }

                    if (NombreFisico.StartsWith("M"))
                    {
                        return TipoAdjunto.Multimedia;
                    }
                }

                return TipoAdjunto.None;
            }
        }
    }

    public enum TipoAdjunto
    {
        Imagen, Archivo, Texto, Multimedia, None
    }
}
