﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AireMobileApp.Frontend.Proxy.DataAccess.Models
{
    /// <summary>
    /// ViewModel para la búsqueda
    /// </summary>
    public class BusquedaViewModel
    {
        /// <summary>
        /// Página Actual
        /// </summary>
        public int PaginaActual { get; set; }

        /// <summary>
        /// Total de items en el resultado
        /// </summary>
        public long TotalItems { get; set; }

        /// <summary>
        /// Total de páginas
        /// </summary>
        public int TotalPaginas { get; set; }

        /// <summary>
        /// Documentos
        /// </summary>
        public IList<DocumentoModel> Documentos { get; set; }
    }
}
