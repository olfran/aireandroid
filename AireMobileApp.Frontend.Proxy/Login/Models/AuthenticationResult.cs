﻿// By Olfran Jiménez <olfran@gmail.com>

using System;
using Newtonsoft.Json;

namespace AireMobileApp.Frontend.Proxy.Login.Models
{
    /// <summary>
    /// Resultado de autenticación
    /// </summary>
    public class AuthenticationResult
    {
        public string Token { get; set; }
        public string Username { get; set; }
        public DateTime Duration { get; set; }

        [JsonIgnore]
        public bool Ok { get; set; }
    }
}
