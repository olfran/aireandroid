﻿// By Olfran Jiménez <olfran@gmail.com>

using AireMobileApp.Frontend.Proxy.Login.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace AireMobileApp.Frontend.Proxy.Login
{
    /// <summary>
    /// Proxy para integrar la autenticación con el web service
    /// </summary>
    public class LoginAire
    {
        readonly string _url;
        readonly string _authUrl;

        /// <summary>
        /// La base url debe estar sin la barra final "/"
        /// Ej. https://example.com
        /// </summary>
        /// <param name="url"></param>
        public LoginAire(string url)
        {
            _url = url;
            _authUrl = _url + "/api/auth/token?username={0}&password={1}";
        }

        /// <summary>
        /// Obtener el Token del web service
        /// </summary>
        /// <param name="username">Nombre de usuario</param>
        /// <param name="password">Contraseña</param>
        /// <returns>AuthenticationResult</returns>
        public async Task<AuthenticationResult> LoginAsync(string username, string password)
        {
            AuthenticationResult result = new AuthenticationResult();
            HttpClient client = new HttpClient();

            var response = await client.GetAsync(string.Format(_authUrl, username, password));
            string json = await response.Content.ReadAsStringAsync();
            var authResult = JsonConvert.DeserializeObject<AuthenticationResult>(json);

            if (response.IsSuccessStatusCode)
            {
                result.Ok = true;
                result.Token = authResult.Token;
                result.Username = authResult.Username;
            }

            return result;
        }
    }
}
